import React from 'react';
import { injectStripe } from 'react-stripe-elements';

import CardSection from './CardSection';

class CheckoutForm extends React.Component {
    handleSubmit = (ev) => {
        console.log(this.props.stripe, 'this.props.stripe');
        
        ev.preventDefault();
        if (this.props.stripe) {
            this.props.stripe
            .createToken()
            .then((payload) => {
                console.log('[token]', payload);
                this.props.stripe.createSource({
                    type: 'three_d_secure',
                    amount: 1099,
                    currency: "eur",
                    three_d_secure: {
                        card: payload.token.card.id
                    },
                    redirect: {
                      return_url: "https://stripe.com/docs/testing#webhooks"
                    }
                }).then(function (result) {
                    console.log('[source]', result);
                });
            });
        } else {
            console.log("Stripe.js hasn't loaded yet.");
        }
    };

    render() {
        return (
        <form onSubmit={this.handleSubmit}>
            <CardSection />
            <button>Confirm order</button>
        </form>
        );
    }
}

export default injectStripe(CheckoutForm);