import React, { Component } from 'react';
import { StripeProvider, Elements } from 'react-stripe-elements';

import CheckoutForm from './CheckoutForm';

class App extends Component {
  render() {
    return (
      <StripeProvider apiKey="pk_test_lnqeomCbifPUHyUvuEXWFBis00VcZ9i72A">
        <Elements>
          <CheckoutForm />
        </Elements>
      </StripeProvider>
    );
  }
}

export default App;
